<?php
/**
 * k4 Coupon Master plugin for Craft CMS 3.x
 *
 * manage coupons
 *
 * @link      https://www.kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4mailspooler\controllers;

use craft\helpers\App;
use craft\helpers\Console;
use craft\helpers\MailerHelper;
use craft\mail\Mailer;
use craft\mail\Message;
use craft\mail\transportadapters\BaseTransportAdapter;
use craft\mail\transportadapters\Smtp;
use craft\models\MailSettings;
use craft\web\Response;
use DateTime;
use k4\k4couponmaster\events\ImportEvent;
use k4\k4couponmaster\K4CouponMaster;

use Craft;
use craft\web\Controller;
use k4\k4couponmaster\services\CouponsService;
use k4\k4couponmaster\services\ImportsService;
use k4\k4mailspooler\mail\SpoolerAdapter;
use Kint\Kint;
use modules\k4sitesympany\K4SiteSympany;
use Swift_Transport_SpoolTransport;

/**
 * @author    Christian Hiller
 * @package   K4CouponMaster
 * @since     1.0.0
 */
class TestController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = [];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionIndex()
    {
	    $settings = Craft::$app->getProjectConfig()->get('email') ?? [];
	    
	    Kint::dump($settings);
	    die();
	    
	    $spoolerSettings = $this->getSpoolerSettings($settings);
	    $mailer = Craft::createObject(App::mailerConfig($spoolerSettings));
	    $transport = $mailer->getTransport();
	
	    $realMailerSettings = $this->getMailerSettings($settings);
	    $realMailer = Craft::createObject(App::mailerConfig($realMailerSettings));
	    $realTransport = $realMailer->getTransport();
	
	    $sent = $this->recoverSpool($transport,$realTransport,$spoolerSettings->transportSettings);
	    
	    return $this->asJson([$sent]);
    }
	
	private function getSpoolerSettings($settings)
	{
		$spoolerSettings = new MailSettings($settings);
		$spoolerSettings->transportSettings = array(
			"spoolerMessageLimit"   => $settings["transportSettings"]["spoolerMessageLimit"],
			"spoolerTimeLimit"      => $settings["transportSettings"]["spoolerTimeLimit"],
			"spoolerRecoverTimeOut" => $settings["transportSettings"]["spoolerRecoverTimeOut"],
		);
		
		return $spoolerSettings;
	}
	
	private function getMailerSettings($settings)
	{
		$realMailerSettings = new MailSettings($settings);
		$realMailerSettings->transportType = Smtp::class;
		$realMailerSettings->transportSettings = array(
			"host"              => $settings["transportSettings"]["host"],
			"port"              => $settings["transportSettings"]["port"],
			"useAuthentication" => $settings["transportSettings"]["useAuthentication"],
			"username"          => $settings["transportSettings"]["username"],
			"password"          => $settings["transportSettings"]["password"],
			"encryptionMethod"  => $settings["transportSettings"]["encryptionMethod"],
			"timeout"           => $settings["transportSettings"]["timeout"]
		);
		return $realMailerSettings;
	}
 
	private function recoverSpool(\Swift_Transport $transport,\Swift_Transport $realTransport, $settings)
	{
		if ($transport instanceof \Swift_Transport_SpoolTransport) {
			$spool = $transport->getSpool();
			
			if ($spool instanceof \Swift_ConfigurableSpool) {
				if (!empty($settings["spoolerMessageLimit"])) {
					$spool->setMessageLimit($settings["spoolerMessageLimit"]);
				}
				if (!empty($settings["spoolerTimeLimit"])) {
					$spool->setTimeLimit($settings["spoolerTimeLimit"]);
				}
			}
			
			if ($spool instanceof \Swift_FileSpool) {
				if (!empty($settings["spoolerRecoverTimeOut"])) {
					$spool->recover($settings["spoolerRecoverTimeOut"]);
				} else {
					$spool->recover();
				}
			}
			
			$sent = $spool->flushQueue($realTransport);
			
			return $sent;
		}
	}
	

	
}
