<?php
/**
 * @link      https://craftcampaign.com
 * @copyright Copyright (c) PutYourLightsOn
 */

namespace k4\k4mailspooler\console\controllers;

use Craft;
use craft\helpers\App;
use craft\helpers\Console;
use craft\helpers\MailerHelper;
use craft\mail\Mailer;
use craft\mail\transportadapters\BaseTransportAdapter;
use craft\mail\transportadapters\Smtp;
use craft\models\MailSettings;
use k4\k4mailspooler\mail\SpoolerAdapter;
use Throwable;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Allows you to send spooled mails.
 *
 * @author    Christian Hiller
 * @package   k4 Mail Spooler
 * @since     1.0
 *
 * Based on symfony swiftmailer-bundle console command
 * https://github.com/symfony/swiftmailer-bundle/blob/master/Command/SendEmailCommand.php
 */
class SpoolController extends Controller
{
    // Public Methods
    // =========================================================================

    /**
     * Send all spooled emails
     *
     * @return int
     * @throws Throwable
     */
    public function actionSend(): int
    {
		$this->processMailer();
		
        return ExitCode::OK;
    }
	
	private function processMailer()
	{
		
		$this->stdout(Craft::t('k4-mail-spooler', 'Processing mails...').PHP_EOL, Console::FG_GREEN);
		
		$settings = Craft::$app->getProjectConfig()->get('email') ?? [];
		if (empty($settings)) return $this->stdout(Craft::t('k4-mail-spooler', 'No email settings found...').PHP_EOL, Console::FG_RED);
		
		if ("k4\k4mailspooler\mail\SpoolerAdapter" != $settings["transportType"])
		{
			return $this->stdout(Craft::t('k4-mail-spooler', 'No spooler adapter active...').PHP_EOL, Console::FG_RED);
		}
		
		$spoolerSettings = $this->getSpoolerSettings($settings);
		$mailer = Craft::createObject(App::mailerConfig($spoolerSettings));
		if (empty($mailer)) return $this->stdout(Craft::t('k4-mail-spooler', 'No valid transport found...').PHP_EOL, Console::FG_RED);
		$transport = $mailer->getTransport();
		if (empty($transport)) return $this->stdout(Craft::t('k4-mail-spooler', 'No valid transport found...').PHP_EOL, Console::FG_RED);
		
		$realMailerSettings = $this->getMailerSettings($settings);
		$realMailer = Craft::createObject(App::mailerConfig($realMailerSettings));
		if (empty($realMailer)) return $this->stdout(Craft::t('k4-mail-spooler', 'No valid transport found...').PHP_EOL, Console::FG_RED);
		$realTransport = $realMailer->getTransport();
		if (empty($realTransport)) return $this->stdout(Craft::t('k4-mail-spooler', 'No valid transport found...').PHP_EOL, Console::FG_RED);
		
		$sent = $this->recoverSpool($transport,$realTransport,$spoolerSettings->transportSettings);
		
		$this->stdout(Craft::t('k4-mail-spooler', '{count} emails sent',['count'=> $sent]).PHP_EOL, Console::FG_GREEN);
		
	}
	
	private function getSpoolerSettings($settings)
	{
		$spoolerSettings = new MailSettings($settings);
		$spoolerSettings->transportSettings = array(
			"spoolerMessageLimit"   => $settings["transportSettings"]["spoolerMessageLimit"],
			"spoolerTimeLimit"      => $settings["transportSettings"]["spoolerTimeLimit"],
			"spoolerRecoverTimeOut" => $settings["transportSettings"]["spoolerRecoverTimeOut"],
		);
		
		return $spoolerSettings;
	}
	
	private function getMailerSettings($settings)
	{
		$realMailerSettings = new MailSettings($settings);
		$realMailerSettings->transportType = Smtp::class;
		$realMailerSettings->transportSettings = array(
			"host"              => $settings["transportSettings"]["host"],
			"port"              => $settings["transportSettings"]["port"],
			"useAuthentication" => $settings["transportSettings"]["useAuthentication"],
			"username"          => $settings["transportSettings"]["username"],
			"password"          => $settings["transportSettings"]["password"],
			"encryptionMethod"  => $settings["transportSettings"]["encryptionMethod"],
			"timeout"           => $settings["transportSettings"]["timeout"]
		);
		return $realMailerSettings;
	}
	
	private function recoverSpool(\Swift_Transport $transport,\Swift_Transport $realTransport, $settings)
	{
		if ($transport instanceof \Swift_Transport_SpoolTransport) {
			$spool = $transport->getSpool();
			
			if ($spool instanceof \Swift_ConfigurableSpool) {
				if (!empty($settings["spoolerMessageLimit"])) {
					$spool->setMessageLimit($settings["spoolerMessageLimit"]);
				}
				if (!empty($settings["spoolerTimeLimit"])) {
					$spool->setTimeLimit($settings["spoolerTimeLimit"]);
				}
			}
			
			if ($spool instanceof \Swift_FileSpool) {
				if (!empty($settings["spoolerRecoverTimeOut"])) {
					$spool->recover($settings["spoolerRecoverTimeOut"]);
				} else {
					$spool->recover();
				}
			}
			
			$sent = $spool->flushQueue($realTransport);
			
			return $sent;
		}
	}
	
	
}
