<?php
/**
 * @link      https://github.com/putyourlightson/craft-amazon-ses
 * @copyright Copyright (c) PutYourLightsOn
 */

namespace k4\k4mailspooler;

use craft\events\RegisterUrlRulesEvent;
use craft\web\UrlManager;
use k4\k4mailspooler\mail\AmazonSesAdapter;

use craft\base\Plugin;
use craft\events\RegisterComponentTypesEvent;
use craft\helpers\MailerHelper;
use k4\k4mailspooler\mail\SpoolerAdapter;
use yii\base\Event;

class K4MailSpooler extends Plugin
{
    /**
     * @var K4MailSpooler
     */
    public static $plugin;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        self::$plugin = $this;

        Event::on(MailerHelper::class, MailerHelper::EVENT_REGISTER_MAILER_TRANSPORT_TYPES,
            function(RegisterComponentTypesEvent $event) {
                $event->types[] = SpoolerAdapter::class;
            }
        );
	
	    Event::on(
		    UrlManager::class,
		    UrlManager::EVENT_REGISTER_CP_URL_RULES,
		    function (RegisterUrlRulesEvent $event) {
			    $event->rules = array_merge($event->rules, $this->getCpRoutes());
		    }
	    );
	
    }
	
	/**
	 * Returns the CP routes
	 *
	 * @return array
	 */
	protected function getCpRoutes(): array
	{
		return [

			'k4-mail-spooler/test'                        => 'k4-mail-spooler/test/index',
		];
	}
	
}
