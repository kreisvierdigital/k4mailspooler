<?php
	
	namespace k4\k4mailspooler\mail;
	
	use Craft;
	use craft\behaviors\EnvAttributeParserBehavior;
	use craft\mail\transportadapters\BaseTransportAdapter;
	use Swift_FileSpool;
	
	/**
	 * SpoolerAdapter implements a spooled SMTP transport adapter into Craft’s mailer.
	 *
	 * @author Christian Hiller <christian.hiller@kreisvier.ch>
	 */
	class SpoolerAdapter extends BaseTransportAdapter
	{
		// Static
		// =========================================================================
		
		/**
		 * @inheritdoc
		 */
		public static function displayName(): string
		{
			return 'k4 Mail Spooler';
		}
		
		// Properties
		// =========================================================================
		
		/**
		 * @var string|null The host that should be used
		 */
		public $host;
		
		/**
		 * @var string|null The port that should be used
		 */
		public $port;
		
		/**
		 * @var bool|null Whether to use authentication
		 */
		public $useAuthentication;
		
		/**
		 * @var string|null The username that should be used
		 */
		public $username;
		
		/**
		 * @var string|null The password that should be used
		 */
		public $password;
		
		/**
		 * @var string|null The encryption method that should be used, if any (ssl or tls)
		 */
		public $encryptionMethod;
		
		/**
		 * @var string The timeout duration (in seconds)
		 */
		public $timeout = 10;
		
		/**
		 * @var string|null The port that should be used
		 */
		public $spoolerMessageLimit;
		
		/**
		 * @var string|null The port that should be used
		 */
		public $spoolerTimeLimit;
		
		/**
		 * @var string|null The port that should be used
		 */
		public $spoolerRecoverTimeOut;
		
		// Public Methods
		// =========================================================================
		
		/**
		 * @inheritdoc
		 */
		public function behaviors()
		{
			return [
				'parser' => [
					'class' => EnvAttributeParserBehavior::class,
					'attributes' => [
						'host',
						'port',
						'username',
						'password',
					],
				],
			];
		}
		
		/**
		 * @inheritdoc
		 */
		public function attributeLabels()
		{
			return [
				'host' => Craft::t('app', 'Host Name'),
				'port' => Craft::t('app', 'Port'),
				'useAuthentication' => Craft::t('app', 'Use authentication'),
				'username' => Craft::t('app', 'Username'),
				'password' => Craft::t('app', 'Password'),
				'encryptionMethod' => Craft::t('app', 'Encryption Method'),
				'timeout' => Craft::t('app', 'Timeout'),
			];
		}
		
		/**
		 * @inheritdoc
		 */
		public function rules()
		{
			$rules = parent::rules();
			$rules[] = [['host'], 'trim'];
			$rules[] = [['host', 'port', 'timeout'], 'required'];
			$rules[] = [
				['username', 'password'],
				'required',
				'when' => function($model) {
					/** @var self $model */
					return (bool)$model->useAuthentication;
				}
			];
			$rules[] = [['encryptionMethod'], 'in', 'range' => ['tls', 'ssl']];
			$rules[] = [['timeout'], 'number', 'integerOnly' => true];
			return $rules;
		}
		
		/**
		 * @inheritdoc
		 */
		public function getSettingsHtml()
		{
			return Craft::$app->getView()->renderTemplate('k4-mail-spooler/_settings.twig', [
				'adapter' => $this,
				''
			]);
		}
		
		/**
		 * @inheritdoc
		 * @throws \Swift_IoException
		 */
		public function defineTransport()
		{
			$config = [
				'class' => \Swift_SpoolTransport::class,
				'constructArgs' => [
					[
						'class' => Swift_FileSpool::class,
						'constructArgs' => [Craft::getAlias('@runtime/mail_spool')],
					]
				]
			];
			
			return $config;
		}
	}
