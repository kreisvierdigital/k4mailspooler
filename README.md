# k4 Mail Spooler

The k4 mail spooler implements a Swift_SpoolTransport::class Adapter using the Swift_FileSpool::class.

## Documentation

Mails sent by Craft when choosing the adapter get spooled into 

    @runtime/mail_spool

From there the plugin provides a configurable craft console command for sending the mails, that can be used e.g. in cron job configuration

    ./craft k4-mail-spooler/spool/send
    
The code was adapted from `symfony/swiftmailer-bundle`

https://github.com/symfony/swiftmailer-bundle/blob/master/Command/SendEmailCommand.php

## License

This plugin is licensed for free under the MIT License.

## Requirements

Craft CMS 3.1.0 or later.

## Installation

To install the plugin, implement it into your `composer.json` under `repositories`

        {
          "type": "vcs",
          "url": "git clone https://kreisvier@bitbucket.org/kreisvierdigital/k4mailspooler.git"
        }

and then 

        composer require k4/k4-mail-spooler

<small>Created by [kreisvier](https://kreisvier.ch/).</small>
